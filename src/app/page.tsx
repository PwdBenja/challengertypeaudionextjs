'use client';

import { transcript } from "@/data/data";
import AUDIO from '@/data/audio.wav';
import { useRef, useState } from "react";

export interface Message {
    content: string;
    role: 'agent' | 'user';
    start: number;
    end: number;
}

export default function Home() {
    const [progress, setProgress] = useState<number>(0)
    
    const audio = useRef<HTMLAudioElement>(null);
    const match = useRef<Message>(null);

    function handleClick(time : number) {
        audio.current!.currentTime = time
        audio.current?.play()
    }

    function handleTimeChange (time :  number) {
        setProgress(time);

        const newMatch = transcript.findLast((message) => {
            return message.start < progress
        })

        if (newMatch !== match.current) {
            document.getElementById(String(match.current?.start))?.scrollIntoView({
                behavior: 'smooth',
                block: 'center',
            })
        }
    }

    return (
        <section className="grid gap-4">
            <div className="grid gap-6  ">
            {
                transcript.map(({ start, content, role }) => (
                    <button 
                        id={String(start)}
                        className={`text-left rounded p-4 max-w-[90%] 
                            ${role == 'user' ? `justify-self-end
                             bg-neutral-700` : `bg-neutral-800`}
                             ${progress < start ? 'opacity-50': ''}`}
                        key={ start }
                        type="button"
                        onClick={() => handleClick(start)}>
                        { content }
                    </button>
                ))
            }
            </div>
            <audio 
                ref={audio} 
                onTimeUpdate={(event) => handleTimeChange(event.currentTarget.currentTime)} 
                className={` sticky bottom-0 justify-self-center max-w-[600px] w-full`} 
                src={AUDIO} 
                controls 
            />
        </section>
    );
}